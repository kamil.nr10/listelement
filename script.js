let size = 10;
let orderElement = 1;

const init = () => {
    const btn = document.createElement('button');
    document.body.appendChild(btn);
    btn.textContent = "Stwórz 10 elementów";
    const btnReset = document.createElement('button');
    document.body.appendChild(btnReset);
    btnReset.textContent = "Wyczyść";
    const ul = document.createElement('ul');
    document.body.appendChild(ul);
    ul.style.listStyle = "none";
    btn.addEventListener('click', createliElements);
    btnReset.addEventListener('click', cleanList);

}

const createliElements = () => {
    for (let i = 0; i < 10; i++) {
        const liElement = document.createElement('li');
        document.querySelector('ul').appendChild(liElement);
        liElement.textContent = `Element ${orderElement++}`;
        liElement.style.fontSize = `${size++}px`;
    }
}

const cleanList = () => {
    document.querySelector('ul').textContent = "";
    size = 10;
    orderElement = 1;
}


init();